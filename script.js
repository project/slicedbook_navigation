// JavaScript Document
function slicedbook_navigation_toggle_menu(objID) {
	var display = $("." + objID).next().css('display');
	display = (display == 'block')?'none': 'block';
	$("." + objID).next().css('display',display);
	$("." + objID).toggleClass('expanded');
	$("." + objID).toggleClass('collapsed');
	return false;
}

$(document).ready(function(){
	$(".sbn_link").click(function(){
		return slicedbook_navigation_toggle_menu($(this).parent().attr('id'));
	}).dblclick(function(){
		window.location = this.href;
	});
	//$("#block-slicedbook_navigation-1 div.content li.collapsed").parents('ul').css('display','block').prev().toggleClass('collapsed').toggleClass('expanded');
	//$("div.content a.active").parents('ul').css('display','block').prev().toggleClass('collapsed').toggleClass('expanded');
});	